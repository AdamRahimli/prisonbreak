﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Olme : MonoBehaviour {

    public float can;
    private bool toxunma;
    public GameObject camera;
    public GameObject panel;
    public GameObject canFonu;
    private float sup;


	void Start () {
        sup = 0;
        toxunma = false;
        can = 100;
	}
	
	// Update is called once per frame
	void Update () {
		if(toxunma == false && can <= 100)
        {
            can += 0.05f;
        }
        if (can < 0)
        {
            Time.timeScale = 0;
            camera.SetActive(true);
            if (sup < 30)
            {
                camera.transform.localPosition = new Vector3(0.0f, sup, 0.0f);
                sup += 0.5f;
            }
            else
            {
                panel.SetActive(true);
            }
            
        }
        canFonu.transform.localScale = new Vector3(1.0f, can / 100, 1.0f);
    }
    void OnCollisionExit(Collision collision)
    {
        toxunma = false;
    }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("police"))
        {
            toxunma = true;
            can -= 0.5f;
        }
        
    }
    void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("police"))
        {
            toxunma = true;
            can -= 0.5f;
        }
    }

}
