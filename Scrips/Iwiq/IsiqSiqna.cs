﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsiqSiqna : MonoBehaviour {

    public static bool hereket;
    public GameObject[] isiq;

    void Start()
    {
        hereket = false;
    }

    public void hereketOn()
    {
        hereket = true;
        Debug.Log(hereket);
    }
    public void hereketOff()
    {
        hereket = false;
    }

    public void isiqOn()
    {
        for(int i = 0; i < 34; i++)
        {
            isiq[i].SetActive(true);
        }
    }
    public void isiqOff()
    {
        for (int i = 0; i < 34; i++)
        {
            isiq[i].SetActive(false);
        }
    }

    void Update()
    {
        Debug.Log(hereket);
        if (hereket)
        {     
            isiqOn();
        }
        else
        {
            isiqOff();
        }  
    }


}
