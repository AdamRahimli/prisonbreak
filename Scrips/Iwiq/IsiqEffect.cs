﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsiqEffect : MonoBehaviour {

    public GameObject isiqYanan;
    public GameObject isiqSonen;
    private int sup;

    // Use this for initialization
    void Start () {
        isiqYanan.SetActive(true);
        isiqSonen.SetActive(false);
        sup = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if (sup < 3)
        {
            isiqYanan.SetActive(false);
            isiqSonen.SetActive(true);
            sup++;
        }else if (sup < 5)
        {
            isiqYanan.SetActive(true);
            isiqSonen.SetActive(false);
            sup++;
        }
        else if (sup < 6)
        {
            isiqYanan.SetActive(false);
            isiqSonen.SetActive(true);
            sup++;
        }
        else if (sup < 7)
        {
            isiqYanan.SetActive(true);
            isiqSonen.SetActive(false);
            sup++;
        }
        else if (sup < 10)
        {
            isiqYanan.SetActive(false);
            isiqSonen.SetActive(true);
            sup++;
        }
        else if (sup < 11)
        {
            isiqYanan.SetActive(true);
            isiqSonen.SetActive(false);
            sup++;
        }
        else if (sup < 30)
        {
            isiqYanan.SetActive(false);
            isiqSonen.SetActive(true);
            sup++;
        }
        else if (sup < 40)
        {
            isiqYanan.SetActive(true);
            isiqSonen.SetActive(false);
            sup++;
        }
        else
        {
            sup = 0;
        }
	}
}
