﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AcarDonme : MonoBehaviour {

    public GameObject yazi;
    private float mesafe;
    public Transform player;

	void Start () {
        yazi.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        mesafe = Vector3.Distance(transform.position, player.position);
      //  Debug.Log(mesafe);
        if (mesafe < 5)
        {
            yazi.SetActive(true);
        }
        else
        {
            yazi.SetActive(false);
        }


        transform.Rotate(0.0f, 30 * Time.deltaTime, 0.0f);
	}
}
