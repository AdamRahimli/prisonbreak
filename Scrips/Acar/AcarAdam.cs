﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AcarAdam : MonoBehaviour {

    public GameObject tool;
    public GameObject toolObject;

    public GameObject toolOpen;
    public GameObject toolClose;

    public GameObject mainOpen;
    public GameObject mainClose;

    public GameObject doorOpen;
    public GameObject doorClose;

    public GameObject mainKey;
    public GameObject doorKey;

    public GameObject mainKeyObject;
    public GameObject doorKeyObject;

    private bool mainKeyPlace;
    private bool doorKeyPlace;
    private bool toolPlace;

    void Start()
    {
        toolClose.SetActive(true);
        toolOpen.SetActive(false);

        tool.SetActive(false);
        toolObject.SetActive(true);

        doorClose.SetActive(true);
        doorOpen.SetActive(false);

        mainClose.SetActive(true);
        mainOpen.SetActive(false);

        mainKeyObject.SetActive(true);
        doorKeyObject.SetActive(true);

        mainKeyPlace = false;
        doorKeyPlace = false;
        toolPlace = false;

        mainKey.SetActive(false);
        doorKey.SetActive(false);
    }


    void Update()
    {
        if (mainKeyPlace)
        {
            mainKey.SetActive(true);
        }
        if (doorKeyPlace)
        {
            doorKey.SetActive(true);
        }
        if (toolPlace)
        {
            tool.SetActive(true);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "Main Key")
        {
            mainKeyObject.SetActive(false);
            mainKeyPlace = true;
        }
        
        if(other.gameObject.name == "Door Key")
        {
            doorKeyObject.SetActive(false);
            doorKeyPlace = true;
        }

        if(other.gameObject.name == "Tool")
        {
            toolObject.SetActive(false);
            toolPlace = true;
        }
        
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Door Close" && doorKeyPlace)
        {
            doorClose.SetActive(false);
            doorOpen.SetActive(true);
        }
        if (collision.gameObject.name == "Main Close" && mainKeyPlace)
        {
            mainClose.SetActive(false);
            mainOpen.SetActive(true);
        }
        if(collision.gameObject.name == "Qapi Close" && toolPlace)
        {
            toolClose.SetActive(false);
            toolOpen.SetActive(true);
        }
    }

}
