﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PoliceFollowNew : MonoBehaviour {

    public Transform player,view1,view2;
    private NavMeshAgent police;
    public float mesafe;
    private bool sup,sup2,sup3;

    public bool hereket;
    IsiqSiqna isiqlar = new IsiqSiqna();

    void Start () {
        sup3 = false;
        sup2 = false;
        sup = false;
        police = GetComponent<NavMeshAgent>();
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 uzunluq = player.position - transform.position;
        float bucaq = Vector3.Angle(uzunluq, transform.forward);

        
        mesafe = Vector3.Distance(transform.position, player.position);
        if (mesafe <= 10 && bucaq <35)
        {
            police.SetDestination(player.position);
            sup2 = true;

            sup3 = true;
            isiqlar.hereketOn();
        }
        else if(mesafe < 5 && sup2 == true)
        {
            police.SetDestination(player.position);
            isiqlar.hereketOn();
            sup3 = true;
        }
        else
        {
            float l1 = Vector3.Distance(transform.position, view1.position);
            float l2 = Vector3.Distance(transform.position, view2.position);
                if (l1 > 1 && sup==false)
                {
                    police.SetDestination(view1.position);
                }
                else
                {
                    sup = true;
                }

                if (l2 > 1 && sup==true)
                {
                    police.SetDestination(view2.position);
                }
                else
                {
                    sup = false;
                } 
            sup2 = false;
            if (sup3)
            {
                isiqlar.hereketOff();
                sup3 = false;
            }
            
        }
    }
}
