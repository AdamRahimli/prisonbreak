﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoliceFollow : MonoBehaviour {

   
   
    public Transform player;
    public float suret, mesafe;
    public bool hereket, vurmaq;
    public float can;
    public bool wall;
    private int sup;
    private int sup2;
    public GameObject[] siqOn;
    public GameObject[] siqOff;

    void Start () {
        
        for(int i = 0; i < 34; i++)
        {
            siqOff[i].SetActive(true);
            siqOn[i].SetActive(false);
        }
        sup2 = 0;
        sup = 0;
        can = 100;
	}
	
	
	void Update () {
        Vector3 uzunluq = player.position - transform.position;
        float bucaq = Vector3.Angle(uzunluq, transform.forward);

        
        mesafe = Vector3.Distance(transform.position,player.position);
        if(mesafe < 10 && bucaq < 30)
        {
            hereket = true;
            transform.LookAt(player);
        }
        else
        {
            hereket = false;
        }
        
        if (hereket)
        {
            for (int i = 0; i < 34 && sup2%7==0; i++)
            {
                siqOff[i].SetActive(false);
                siqOn[i].SetActive(true);
            }
            for (int i = 0; i < 34 && sup2%13==0; i++)
            {
                siqOff[i].SetActive(true);
                siqOn[i].SetActive(false);
            }

            sup2++;
            sup = 0;
            transform.position = Vector3.MoveTowards(transform.position, player.position, suret * Time.deltaTime);
        }
        else
        {
           
            sup2 = 0;
            
            if (sup < 70)
            {
                transform.Rotate(0.0f, 13*Time.deltaTime , 0.0f);
                sup++;
            }
            else if(sup < 210)
            {
                transform.Rotate(0.0f, -13*Time.deltaTime , 0.0f);
                sup++;
            }
            else if (sup < 280)
            {
                transform.Rotate(0.0f, 13 * Time.deltaTime, 0.0f);
                sup++;
            }
            else
            {
                sup = 0;
            }
            
          //  Debug.Log(transform.rotation.y);
        }
	}
    
}
