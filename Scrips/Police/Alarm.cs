﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alarm : MonoBehaviour {

    private AudioSource alarm;

    
    // Use this for initialization
    void Start () {
        alarm = GetComponent<AudioSource>();
       
    }

    // Update is called once per frame
    void Update()
    {
        alarm.Play();        
    }
}
