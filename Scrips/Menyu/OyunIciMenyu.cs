﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OyunIciMenyu : MonoBehaviour {

    public GameObject panel;
    private int sup;
    private int supp;
    private bool sup2;
    private bool sup3;
    private bool sup4;

  

	void Start () {
        sup4 = false;
        sup3 = false;
        sup2 = false;
        supp = 0;
        sup = 0;
        panel.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.Escape) && sup3 == false)
        {
            panel.SetActive(true);
            Time.timeScale = 0;

            sup2 = true;
        }
        else if (Input.GetKey(KeyCode.Escape) && sup3 == true)
        {
            panel.SetActive(false);
            Time.timeScale = 1;

            sup4 = true;
        }

        if (supp < 15 && sup4 == true)
        {
            supp++;
        }
        else if (supp == 15)
        {
            sup4 = false;
            sup3 = false;
            supp = 0;
        }

        if (sup < 15 && sup2 == true)
        {
            sup++;
        }
        else if(sup==15)
        {
            sup2 = false;
            sup3 = true;
            sup = 0;
        }


        
    }

   
    public void PanelDavam()
    {
        panel.SetActive(false);
        Time.timeScale = 1;
    
        sup3 = false;
    }
    public void PanelExit()
    {
        Application.Quit();
    }
}
