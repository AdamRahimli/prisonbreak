﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenyuGecis : MonoBehaviour {

	public void SceneNext(string sehne)
    {
        SceneManager.LoadScene(sehne);
    }

    public void SceneClose()
    {
        Application.Quit();
    }

}
