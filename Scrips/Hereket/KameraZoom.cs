﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KameraZoom : MonoBehaviour {

    private float zoom;
    public float zoomSpeed=2;
    public float zoomMax = -5;
    public float zoomMin = -3;
    public Transform player;
    private bool checkWall;
    private float zMin;
    private float oldTrs;
    private float sup;
    private int sup2;
    public GameObject silah;
    private float zoom2;
    private bool sup3;
    private int sup4;
    private float sup5;


    void Start () {
   
        sup4 = 0;
        sup3 = false;
        zoom = -3.5f;
        checkWall = false;
        zMin = transform.localPosition.z + zoom2;
        oldTrs = transform.localPosition.z;
    }
	
	// Update is called once per frame
	void Update () {

        
        sup = transform.localPosition.z;
        if (checkWall == false && sup2 % 2 == 0 && sup2 != 0 && sup4 < 30)
        {
            if (sup < oldTrs)
            {
                sup += 0.05f;
                transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, sup);
            }
            sup3 = false;
            sup4++;
        }
        else
        {
            sup = transform.localPosition.z;
            sup3 = true;
        }

    //    Debug.Log(sup3);
    //    Debug.Log(checkWall);
        if (checkWall == false && sup3 == true)
        {
            zoom += Input.GetAxis("Mouse ScrollWheel") * zoomSpeed;

            if (zoom > zoomMin)
                zoom = zoomMin;
            if (zoom < zoomMax)
                zoom = zoomMax;
            //      Debug.Log(zoom);
            sup5 = transform.localPosition.z;
            if (sup5>zoom)
            {
                sup5 -= 0.05f;
                transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, sup5);
            }
            else
            {
                transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, zoom);
            }
            
        }

        
	}

    private void LateUpdate()
    {
        transform.LookAt(player);
    }
    void OnTriggerEnter(Collider other)
    {
        sup4 = 0;
        oldTrs = transform.localPosition.z;
        sup2++;
        checkWall = true;    
    }

    void OnTriggerExit(Collider other)
    {
        sup4 = 0;
        sup2++;
        checkWall = false;
    }

    void OnTriggerStay(Collider col)
    {

        checkWall = true;
        if (col.gameObject.name == "wall")
        {
            zMin = transform.localPosition.z + zoom2;

            if (transform.localPosition.z > silah.transform.localPosition.z /*- 1.0f*/)
            {
                zMin = 1.7f;
            }
            else
            {

            }
            transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, zMin);

         //   Debug.Log(zMin);
            zoom2 += 0.02f;
        }
    }
}
