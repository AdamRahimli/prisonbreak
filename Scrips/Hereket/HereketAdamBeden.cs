﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HereketAdamBeden : MonoBehaviour
{
    private AudioSource ayaqSesi;
    public float suret;
    public float ms;


    void Start()
    {
        ayaqSesi = GetComponent<AudioSource>();
        ayaqSesi.Stop();
        Time.timeScale = 1;
    }

    void Update()
    {
        if(Input.GetButtonDown("Vertical") || Input.GetButtonDown("Horizontal"))
        {
            ayaqSesi.Play();
        }
        else if(Input.GetButtonUp("Vertical") || Input.GetButtonUp("Horizontal"))
        {
            ayaqSesi.Stop();
        }



        transform.Translate(Input.GetAxis("Horizontal") * suret * Time.deltaTime, 0.0f, Input.GetAxis("Vertical") * suret * Time.deltaTime);
        transform.Rotate(0.0f, Input.GetAxis("Mouse X")*ms*Time.deltaTime, 0.0f);
    }
}
