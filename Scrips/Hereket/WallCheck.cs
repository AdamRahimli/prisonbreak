﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallCheck : MonoBehaviour {

    private float zoom;
    private float zMin;
    private bool checkWall;
    private float oldTrs;
    private float sup;
    private int sup2;
    public GameObject silah;

    public bool divar()
    {
        return checkWall;
    } 

    void Start () {
        zoom = 0.2f;
        zMin = transform.localPosition.z + zoom;
     //   CheckWall = false;
        oldTrs = transform.localPosition.z;
    }
	
	void Update () {
        sup = transform.localPosition.z;
        if (checkWall == false && sup2%2==0 && sup2!=0)
        {
            if(sup < oldTrs)
            {
                sup += 0.1f;
                transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, sup);
            }
        }
        else
        {
            sup = transform.localPosition.z;
        }

        Debug.Log(checkWall);
        
	}

    void OnTriggerEnter(Collider other)
    {
        oldTrs = transform.localPosition.z;
        sup2++;
     //   CheckWall = true;    
    }

    void OnTriggerExit(Collider other)
    {
        sup2++;
    //    CheckWall = false;
    }

    void OnTriggerStay(Collider col)
    {
        if (col.gameObject.name == "wall")
        {
            zMin = transform.localPosition.z + zoom;
            
            if (transform.localPosition.z > silah.transform.localPosition.z-1.0f)
            {
                zMin = 0.0f;
            }
            else
            {
                
            }
            transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, zMin);

        //    Debug.Log(transform.localPosition.z);
            zoom += 0.05f;
        }
    }
}
